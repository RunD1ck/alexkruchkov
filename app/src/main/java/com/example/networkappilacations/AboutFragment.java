package com.example.networkappilacations;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.networkappilacations.entity.Fact;


public class AboutFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView text = view.findViewById(R.id.text);
        TextView textType = view.findViewById(R.id.textType);
        TextView textVersion = view.findViewById(R.id.textVersion);
        TextView textUpdate = view.findViewById(R.id.textUpvotes);

        ImageView catImageView = view.findViewById(R.id.imageCat);
        Animation catAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.cat_anim);
        catImageView.startAnimation(catAnimation);
        Bundle bundle = getArguments();
        Fact fact = (Fact) bundle.getParcelable("facts");
        if (fact != null) {
            text.setText(fact.getText());
            Log.e("Text", fact.getText() + "text");
            textType.setText(fact.getType());
            Log.e("Text", fact.getType() + " textType");
            textVersion.setText(fact.getVersion() + "");
            Log.e("Text", fact.getVersion() + " textVersion");
            textUpdate.setText(fact.getUpvotes() + "");
            Log.e("Text", fact.getUpvotes() + " upvotes");
        }
    }
}