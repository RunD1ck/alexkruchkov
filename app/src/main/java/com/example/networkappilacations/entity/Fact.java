package com.example.networkappilacations.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Fact implements Parcelable {

    @SerializedName("_v")
    public int version;

    @SerializedName("text")
    public String text;
    @SerializedName("upvotes")
    public int upvotes;
    @SerializedName("type")
    public String type;
    public Fact(int version, String text, int upvotes, String type){

        this.version = version;
        this.text = text;
        this.upvotes = upvotes;
        this.type = type;
    }

     public Fact(Parcel in) {
        version = in.readInt();
        text = in.readString();
        upvotes = in.readInt();
        type = in.readString();
    }

    public static final Creator<Fact> CREATOR = new Creator<Fact>() {
        @Override
        public Fact createFromParcel(Parcel in) {
            int version = in.readInt();
            String text = in.readString();
            int upvotes = in.readInt();
            String type = in.readString();
            return new Fact(version,text,upvotes,type);
        }

        @Override
        public Fact[] newArray(int size) {
            return new Fact[size];
        }
    };

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version){
        this.version = version;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text){
        this.text = text;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getUpvotes() {
         return this.upvotes;
    }

    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(version);
        dest.writeString(text);
        dest.writeString(type);
    }
}
