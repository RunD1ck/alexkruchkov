package com.example.networkappilacations.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BaseResponse {
    @SerializedName("all")
    public List<Fact> all;
}
