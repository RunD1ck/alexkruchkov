package com.example.networkappilacations;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.networkappilacations.entity.BaseResponse;
import com.example.networkappilacations.entity.Fact;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainFragment extends Fragment {

    private static final String MY_SETTINGS = "my_settings";
    RecyclerView recyclerView;
    ArrayList<Fact> facts;
    DataAdapter.RecyclerViewClickListener listener;
    public static final String TAG = "com.example.networkappilacations.MainFragment";
    private OnFragmentDataListener fListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        facts = new ArrayList<>();
        setOnClickListener();
        recyclerView = view.findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        DataAdapter adapter = new DataAdapter(getContext(),facts, listener);
        recyclerView.setAdapter(adapter);
        App.getApi().getFacts().enqueue(new Callback<BaseResponse>(){

            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                facts.addAll(response.body().all);
                recyclerView.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
            }
        });
        SharedPreferences sp = getActivity().getSharedPreferences(MY_SETTINGS,
                Context.MODE_PRIVATE);
        boolean hasVisited = sp.getBoolean("hasVisited",false);
        if(!hasVisited) {
            DialogNotification dialogNotification = new DialogNotification();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            dialogNotification.show(transaction, "dialog");
            SharedPreferences.Editor e = sp.edit();
            e.putBoolean("hasVisited",true);
            e.commit();
        }
    }
    private void setOnClickListener() {
        listener = new DataAdapter.RecyclerViewClickListener() {
            @Override
            public void onClick(View v, int position) {
                fListener.onOpenAboutFragment(facts.get(position));
            }
        };
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof OnFragmentDataListener){
            fListener = (OnFragmentDataListener) context;
        }else{
            throw new RuntimeException(context.toString()
                    + "must implement OnFragmentDataListener");
        }
    }
}