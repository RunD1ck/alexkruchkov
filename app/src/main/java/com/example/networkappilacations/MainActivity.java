package com.example.networkappilacations;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.example.networkappilacations.entity.Fact;



public class MainActivity extends AppCompatActivity implements OnFragmentDataListener  {

    private static final String MY_SETTINGS = "my_settings";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
        if(fragment == null){
            fragment = new MainFragment();
            fm.beginTransaction()
                    .add(R.id.fragmentContainer,fragment, MainFragment.TAG)
                    .commit();
        }
        SharedPreferences sp = getSharedPreferences(MY_SETTINGS,
                Context.MODE_PRIVATE);
        boolean hasVisited = sp.getBoolean("hasVisited",false);
        if(!hasVisited) {
            DialogNotification dialogNotification = new DialogNotification();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            dialogNotification.show(transaction, "dialog");
            SharedPreferences.Editor e = sp.edit();
            e.putBoolean("hasVisited",true);
            e.commit();
        }
    }
        public void onOpenAboutFragment(Fact fact){
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragmentContainer);
        if(fragment instanceof MainFragment){
            Fragment fragmentReplace;
            fragmentReplace = new AboutFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelable("facts", fact);
            fragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer,fragmentReplace,MainFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(MainFragment.TAG)
                    .commit();
            fragmentReplace.setArguments(bundle);
        }
    }
}
