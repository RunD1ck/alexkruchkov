package com.example.networkappilacations;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.networkappilacations.entity.Fact;

import java.util.List;

class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder>{

    private static final int TYPE_GREEN = 1;
    private static final int TYPE_ORANGE = 2;
    private LayoutInflater inflater;
    private List<Fact> facts;
    private RecyclerViewClickListener listener;

    DataAdapter(Context context, List<Fact> facts, RecyclerViewClickListener listener){
        this.facts = facts;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }
    @NonNull
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

        View view;
        if(viewType == TYPE_ORANGE) {
            view = inflater.inflate(R.layout.message_list_orange, parent, false);
        }else{
            view = inflater.inflate(R.layout.message_list_green, parent, false);
        }
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position){
        Fact fact = facts.get(position);
        holder.bind(facts.get(position));
    }

    @Override
    public int getItemCount() {
        return  facts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final TextView nameView, textView;

        ViewHolder(View view) {
            super(view);
            nameView = view.findViewById(R.id.name);
            textView = view.findViewById(R.id.text);
            view.setOnClickListener(this);
        }
        public void bind(Fact item){
            nameView.setText(item.type);
            textView.setText(item.text);
        }
        @Override
        public void onClick(View v) {
            listener.onClick(v, getAdapterPosition());
        }
    }
    public interface RecyclerViewClickListener{
        void onClick(View v, int position);
    }

    @Override
    public int getItemViewType(int position) {
        if(facts.get(position).getText().length() > 60){
            return TYPE_GREEN;
        }else{
            return TYPE_ORANGE;
        }
    }
}
