package com.example.networkappilacations.server;

import com.example.networkappilacations.entity.BaseResponse;
import com.example.networkappilacations.entity.Fact;

import java.util.List;


import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    String BASE_URL = "https://cat-fact.herokuapp.com/";

    @GET("facts")
    Call<BaseResponse> getFacts();

}
