package com.example.networkappilacations;

import android.content.Intent;

import com.example.networkappilacations.entity.Fact;

public interface OnFragmentDataListener {
    void onOpenAboutFragment(Fact fact);
}
